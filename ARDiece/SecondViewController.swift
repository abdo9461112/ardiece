//
//  SecondViewController.swift
//  ARDiece
//
//  Created by abdo emad  on 05/12/2023.
//

import UIKit
import ARKit
import SceneKit

    var diceArray = [SCNNode]()

class SecondViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet weak var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sceneView.delegate = self
        
        sceneView.autoenablesDefaultLighting = true
        
        
    
    }
    
    func rollAll(){
        
        if !diceArray.isEmpty{
            for dice in diceArray{
                roll(dice: dice)
            }
        }
    }
    
    
    func roll(dice: SCNNode){
        
        let randomX = Float(arc4random_uniform(4) + 1) * (Float.pi/2)
       
        let randomZ = Float(arc4random_uniform(4) + 1) * (Float.pi/2)
        
        dice.runAction(
            SCNAction.rotateBy(
                x: CGFloat(randomX * 5),
                y: 0,
                z: CGFloat(randomZ * 5),
                duration: 0.5)
        )

    }
    
    
    @IBAction func rollAgain(_ sender: UIBarButtonItem) {
        rollAll()
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        rollAll()
    }
    
    @IBAction func removeAllDices(_ sender: UIBarButtonItem) {
        
        if !diceArray .isEmpty{
            
            for dice in diceArray{
                dice.removeFromParentNode()
            }
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first{
            
            let touchLocation = touch.location(in: sceneView)
            
            let results = sceneView.hitTest(touchLocation, types: .existingPlaneUsingExtent)
            
            
            if let hitResults = results.first{
                
                
                let diceScene = SCNScene(named: "art.scnassets/diceCollada.scn")!

                    if let dicenode = diceScene.rootNode.childNode(withName: "Dice", recursively: true) {
            
                        dicenode.position = SCNVector3(
                            x: hitResults.worldTransform.columns.3.x,
                            y: hitResults.worldTransform.columns.3.y + dicenode.boundingSphere.radius,
                            z:hitResults.worldTransform.columns.3.z
                        )
                        
            
                        sceneView.scene.rootNode.addChildNode(dicenode)
                        
                        diceArray.append(dicenode)
                    
                        roll(dice: dicenode)
                    }
                
               
            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if anchor is ARPlaneAnchor {
            
            let planeAnchor = anchor as! ARPlaneAnchor
            
            let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
            
            let planeNode = SCNNode()
            
            planeNode.position = SCNVector3(x: planeAnchor.center.x, y: 0, z: planeAnchor.center.z)
            
            planeNode.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
            
            let graidMateril = SCNMaterial()
            
            graidMateril.diffuse.contents = UIImage(named: "art.scnassets/grid.png")
            
            plane.materials = [graidMateril]
            
            planeNode.geometry = plane
            
            node.addChildNode(planeNode)
            
            
            
        }else{
            return
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        
        
        configuration.planeDetection = .horizontal
        
        sceneView.session.run(configuration)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
}
